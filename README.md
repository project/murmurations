Murmurations is a new protocol for indexing and retrieving content between different site platforms. Different schemas, or content types are defined. The site that hosts the content pings the (a) murmurations index when the content is updated. The index responds by looking for the json version of the content on the host platform, then indexes it, saving only the core fields. Then an aggregator can read the index, taking only the profiles it is interested in, pinging the host site again or periodically to remain up to date, and storing all the metadata fields. 
This module has a plugin architecture, one plugin for each murmurations schema. It supports just the organisations schema. The mutual_credit module has a plugin to support the complementary_currencies schema, and the smallads module has a plugin to support the offers_wants schema.
So when a new item is saved, this module pings the index and publishes the json version of the file in the murmurations schema format. This module also provides a search page allowing users to filter the content and see it in a list or on a map.
Before using the module, you must know what aggregator you intend to use. It can be a different aggregator for each schema.

## Recommended modules
Murmurations expects to give every object a location, so the geofield module or something similar may be needed for some of the plugins.
However for the only schema included here, Organisation, the coordinates can be entered manually, once only. 

## Configuration

1. Go to Administration > Configuration > Services > Murmurations.
1. Configure the core fields on the default tab.
1. Each available plugin provides another tab with configuration fields. You need to know what aggregator you intend to use.
 (optional)

-  - [Matthew Slater](https://www.drupal.org/u/matslats)

## Issue queue
https://www.drupal.org/project/issues/murmurations
