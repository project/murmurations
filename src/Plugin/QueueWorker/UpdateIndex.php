<?php

namespace Drupal\murmurations\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\Core\Queue\Attribute\QueueWorker;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Updates a feed's items.
 */
#[QueueWorker(
  id: 'murmurations_update_index',
  title: new TranslatableMarkup('Update the murmurations index'),
  cron: ['time' => 30]
)]
class UpdateIndex extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($rel_url) {
    try {
      \Drupal::service('murmurations.index')->reindex($rel_url);
    }
    catch(ConnectException $e) {
      throw new SuspendQueueException('Stopped proccessing queue when murmurations index was not available.');
    }
    catch(\Exception $e) {
      // Items remain in the queue, I presume;
    }
  }

}
