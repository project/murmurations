<?php

namespace Drupal\murmurations\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\Core\Queue\Attribute\QueueWorker;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Updates a feed's items.
 */
#[QueueWorker(
  id: 'murmurations_delete_index',
  title: new TranslatableMarkup('Delete from the murmurations index'),
  cron: ['time' => 30]
)]
class DeleteIndex extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($abs_url) {
    try {
      \Drupal::service('murmurations.index')->deindex($abs_url);
    }
    catch(ConnectException $e) {
      throw new SuspendQueueException('Stopped proccessing queue when murmurations index was not available.');
    }
    catch(\Exception $e) {
      // Items remain in the queue, I presume;
    }
  }

}
