<?php

namespace Drupal\murmurations\Plugin\Murmurations;

use Drupal\Core\Entity\EntityInterface;
use Drupal\murmurations\Plugin\Murmurations\PluginBase;
use Drupal\murmurations\MurmurationsPluginMultipleInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @todo Inject entityTypeManager.
 */
abstract class PluginMultipleBase extends PluginBase implements MurmurationsPluginMultipleInterface {

  /**
   * @var Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var EntityInterface
   */
  protected $entity;

  /**
   *
   * @param $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param $config_factory
   * @param EntityTypeManager $entity_type_manager
   */
  function __construct($configuration, $plugin_id, $plugin_definition, $config_factory, EntityTypeManager $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $config_factory);
    $this->entityTypeManager = $entity_type_manager;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  function needed() : string {
    return $this->getEntityIds() ? '' : t('No items are eligible to be published');
  }

  /**
   * {@inheritDoc}
   */
  function setEntity(EntityInterface|int $entity) : self {
    if (is_int($entity)) {
      $entity_type_id = $this->getPluginDefinition()['entity_type'];
      $this->entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity);
    }
    else {
      $this->entity = $entity;
    }
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  function getPublishPath() : string {
    // Replace the parameter in the publish path
    return preg_replace('/\{[a-z]+\}/', $this->entity->id(), $this->getPluginDefinition()['profile_path']);
  }

  /**
   * {@inheritDoc}
   */
  public function getProfilePaths() : array {
    $paths = [];
    foreach ($this->getEntityIds() as $id) {
      $entity_type_id = $this->getPluginDefinition()['entity_type'];
      $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($id);
      $paths[] = $this->setEntity($entity)->getPublishPath();
    }
    return $paths;
  }

  public function getProfile() : array {
    $profile = [
      'title' => trim($this->entity->label()),
      'primary_url' => $this->entity->toUrl('canonical', ['absolute' => TRUE])->toString()
    ];
    if ($address_value = murmurations_entity_field_type_value($this->entity, 'address')) {
      if ($address_value['administrative_area']) {
        $profile['region'] = $address_value['administrative_area'];
      }
      if ($address_value['locality']) {
        $profile['locality'] = $address_value['locality'];
      }
    }
    return $profile + parent::getProfile();
  }

  /**
   * {@inheritDoc}
   */
  abstract function getEntityIds() : array;

  /**
   * Get coordinates for the $entity
   * @return array
   *   Find the geofield and if not, find the geofield of the owner, and if not, get the site geofield
   */
  function getCoords() : array {
    return displace_point(
      murmurations_geolocate_entity_with_fallback($this->entity),
      $this->configuration['displacement']
    );
  }

  /**
   * {@inheritDoc}
   */
  function publishable() : bool {
    return TRUE;
  }

}
