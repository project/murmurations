<?php

namespace Drupal\murmurations\Plugin\Murmurations;

use Drupal\murmurations\MurmurationsPluginInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase as BasePlugin;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class PluginBase extends BasePlugin implements ContainerFactoryPluginInterface, MurmurationsPluginInterface {

  protected $configFactory;
  protected $murmurationsConfig;

  function __construct($configuration, $plugin_id, $plugin_definition, $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->murmurationsConfig = $config_factory->get('murmurations.settings');
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritDoc}
   */
  function configForm() : array {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  function isReady() : bool {
     return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function setObject() : self {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration($config) {
    $this->configuration = $config + $this->defaultConfiguration();
  }

  /**
   * Gets default configuration for this plugin.
   *
   * @return array
   *   An associative array with the default configuration.
   */
  public function defaultConfiguration() {
    return [
      'displacement' => 3
    ];
  }

  /**
   * {@inheritDoc}
   */
  function needed() : string {
    return '';
  }

  /**
   * {@inheritDoc}
   */
  public function getPublishPath() : string {
    return $this->getPluginDefinition()['profile_path'];
  }

  /**
   * {@inheritDoc}
   */
  public function getProfilePaths() : array {
    return [$this->getPublishPath()];
  }

  /**
   * {@inheritDoc}
   */
  function getProfile() : array {
    global $base_url;
    $fields = [
      'linked_schemas' => [$this->getPluginDefinition()['schema']],
      // todo The following may not be present in all schemas
      'profile_url' => $base_url .'/'. $this->getPublishPath(),
      'primary_url' => $base_url
    ];
    if ($coords = $this->getCoords()) {
      // This is optional, but must not be empty.
      $fields['geolocation'] = $coords;
    }
    if ($locality = $this->murmurationsConfig->get('locality')) {
      $fields['locality'] = $locality;
    }
    if ($region = $this->murmurationsConfig->get('region')) {
      $fields['region'] = $region;
    }
    // NB from Drupal 10.3 this value might be NULL
    if ($country = $this->configFactory->get('system.date')->get('country.default')) {
      $fields['country'] = $country;
    }
    return $fields;
  }

  /**
   * @return array
   *   with float 'lat' and float 'lon'
   */
  function getCoords() : array {
    return $this->murmurationsConfig->get('fallback_point');
  }

  /**
   * {@inheritDoc}
   *
   * @note This should always be overridden
   */
  function renderResult(\stdClass $result) : \Drupal\Core\Render\Markup {
    $m = $result->primary_url .'<br />'. implode(',', array_filter([$result->locality, $result->region, $result->country]));
    return \Drupal\Core\Render\Markup::create($m);
  }

  /**
   * {@inheritDoc}
   */
  function filterFormValues(array $form_values) : array {
    return $form_values;
  }

}
