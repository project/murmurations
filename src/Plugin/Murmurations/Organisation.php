<?php

namespace Drupal\murmurations\Plugin\Murmurations;

use Drupal\murmurations\Plugin\Murmurations\PluginBase;
use Drupal\murmurations\Attribute\Murmurations;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Component\Plugin\ConfigurableInterface;

/**
 * Share's the site data as murmurations Organisation schema
 */
#[Murmurations(
  id: 'organisation',
  label: new TranslatableMarkup('Organisation'),
  schema: 'organizations_schema-v1.0.0',
  profile_path: 'organisation.json',
  config: 'murmurations.site_profile',
  default_aggregator: 'https://index.murmurations.network/v2'
)]
class Organisation extends PluginBase implements ConfigurableInterface {

  const NODE_TYPES = [
    "co-op",
    "social enterprise",
    "community",
    "ecovillage",
    "alliance",
    "network",
    "federation",
    "farm",
    "school"
  ];

  /**
   * {@inheritDoc}
   */
  function needed() : string {
    $needed_fields = [];
    $message = '';
    $required = ['tags', 'node_types'];
    foreach($required as $f) {
      if (!$this->configFactory->get('murmurations.site_profile')->get($f)) {
        $needed_fields[] = $f;
      }
    }
    return $needed_fields ?
      t('The following fields must be populated: @fields.', ['@fields' => implode(', ', $needed_fields)]) :
      '';
  }

  /**
   * {@inheritDoc}
   */
  function getProfile() : array {
    global $base_url;
    // todo inherit some of these properties?
    $siteProfile = $this->configFactory->get('murmurations.site_profile');
    $values = parent::getprofile() + [
      'name' => $this->configFactory->get('system.site')->get('name'),
      'tagline' => $this->configFactory->get('system.site')->get('slogan'),
      'mission' => $siteProfile->get('mission'),
      'node_types' => implode(',', array_values($siteProfile->get('node_types'))),
      'location' => $siteProfile->get('location'),
      'logo' => $base_url . theme_get_setting('logo.url'),
      'networks' => $siteProfile->get('networks'),
      'allies' => $siteProfile->get('allies'),
      'tags' => explode(',',$siteProfile->get('tags')),
      'feed' => $siteProfile->get('feed'),
    ];
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'mission' => '',
      'node_types' => [],
      'location' => '',
      'networks' => '',
      'allies' => '',
      'tags' => '',
      'feed' => ''
    ];
  }

  /**
   * {@inheritDoc}
   */
  function configForm() : array {
    $profile_path = $this->getPluginDefinition()['profile_path'];
    // We don't have a way to intervene after the config is saved, but it will
    // probably rebuild the page anyway.
    // @todo this feels very wrong.
    try {
      \Drupal::service('murmurations.index')->reindex($profile_path);
    }
    catch(\Exception $e){}
    $profile_url = Url::fromUserInput('/'.$profile_path, ['absolute' => TRUE]);
    $config = (object)$this->getConfiguration();
    $node_types = array_combine(SELF::NODE_TYPES, SELF::NODE_TYPES);
    array_walk($node_types, function (&$val){
      $val= ucfirst($val);
    });
    $form['link'] = [
      '#type' => 'item',
      '#title' => t('show schema'),
      '#markup' => Link::fromTextAndUrl($profile_url->toString(), $profile_url)->toString()
    ];

    $form['mission'] = [
      '#title' => $this->t('Mission'),
      '#description' => $this->t("The mission, purpose, or objective of your project or organisation"),
      '#required' => FALSE,
      '#type' => 'textarea',
      '#element_validate' => [[$this, 'max1000']],
      '#weight' => 1,
      '#default_value' => $config->mission,
    ];

    $form['node_types'] = [
      '#title' => $this->t('Organisation or project types'),
      '#description' => $this->t("Examples: co-op, farm, school, community, NGO, etc"),
      '#required' => TRUE,
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => $node_types,
      '#weight' => 2,
      '#default_value' => $config->node_types,
    ];
    $form['location'] = [
      '#title' => $this->t('Location'),
      '#description' => $this->t("The name of a place, e.g. Inverness, Scotland."),
      '#required' => FALSE,
      '#type' => 'textfield',
      '#maxlength' => 1000,
      '#element_validate' => [[$this, 'max1000']],
      '#weight' => 3,
      '#default_value' => $config->location,
    ];

    $form['networks'] = [
      '#title' => $this->t('Networks'),
      '#description' => $this->t("What networks does this organisation or project belong to?"),
      '#required' => FALSE,
      '#type' => 'textfield',
      '#weight' => 6,
      '#default_value' => $config->networks,
    ];
    $form['allies'] = [
      '#title' => $this->t('Allied projects'),
      '#description' => $this->t("Enter the website addresses of allied organisations or projects (comma separated)"),
      '#required' => FALSE,
      '#type' => 'textfield',
      '#weight' => 7,
      '#default_value' => $config->allies,
    ];
    $form['tags'] = [
      '#title' => $this->t('Tags'),
      '#description' => $this->t("The most common tags you use to describe your sector or topic (comma separated)"),
      '#required' => TRUE,
      '#type' => 'textfield',
      '#weight' => 7,
      '#default_value' => $config->tags,
    ];
    $form['feed'] = [
      '#title' => $this->t('RSS feed URL'),
      '#description' => $this->t('If you publish news or content using the RSS format.'),
      '#required' => FALSE,
      '#type' => 'textfield',
      '#weight' => 8,
      '#default_value' => $config->feed,
    ];
    return $form;
  }

  /**
   * Element validation callback.
   *
   * @param type $element
   * @param FormStateInterface $form_state
   */
  public function max1000(&$element, FormStateInterface $form_state) {
    if (strlen($element['#value']) > 1000) {
      $form_state->setError($element, $this->t('Max 1000 chars'));
    }
  }

  /**
   * {@inheritDoc}
   */
  function renderResult(\stdClass $result) : Markup {
    return Markup::create(print_r($result, 1));
  }

  /**
   * Alter the search form to suit the murmurations schema.
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @param array $defaults
   *
   * @todo Declare this in an interface which would be optional.
   */
  function filterFormAlter(array &$form, FormStateInterface $form_state, array $defaults) {
    // NB search form not provided.
  }

  /**
   * {@inheritDoc}
   */
  function filterFormValues(array $values) : array {
    return [];
  }

}
