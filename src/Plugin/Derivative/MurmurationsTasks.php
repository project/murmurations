<?php

namespace Drupal\murmurations\Plugin\Derivative;

use Drupal\murmurations\MurmurationsPluginManager;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Provides map and list tabs for each murmurations plugin
 */
class MurmurationsTasks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  protected $murmPluginManager;

  /**
   * @param MurmurationsPluginManager $murmurations_plugin_manager
   */
  public function __construct(MurmurationsPluginManager $murmurations_plugin_manager) {
    $this->murmPluginManager = $murmurations_plugin_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('plugin.manager.murmurations')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $plugins= $this->murmPluginManager->allInstances();
    foreach($plugins as $id => $plugin) {
      // Tab for settings of each plugin
      $this->derivatives["{$id}_settings"] = [
        'title' => $plugin->getPluginDefinition()['label'],
        'base_route' => 'murmurations.settings',
        'route_name' => "murmurations.settings.plugin",
        'route_parameters' => ['id' => $id],
        'weight' => 2,
        //'id' => "{$id}_settings"
      ] + $base_plugin_definition;

      // Tab for map and list of each plugin's filter page
      $this->derivatives["{$id}_filter_map"] = [
        'title' => 'Map',
        'description' => $this->t('Search ads accross the network'),
        'base_route' => "murmurations.$id.search.map",
        'route_name' => "murmurations.$id.search.map",
        'weight' => 1,
        //'id' => "{$id}_filter_map",
      ] + $base_plugin_definition;
      $this->derivatives["{$id}_filter_list"] = [
        'title' => 'List',
        'description' => $this->t('Search ads accross the network'),
        'base_route' => "murmurations.$id.search.map",
        'route_name' => "murmurations.$id.search.list",
        'weight' => 1,
        //'id' => "{$id}_filter_list",
      ] + $base_plugin_definition;
    }
    return $this->derivatives;
  }

}
