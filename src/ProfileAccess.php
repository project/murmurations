<?php

namespace Drupal\murmurations;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;

/**
 * Determines access to murmurations profiles.
 */
class ProfileAccess implements AccessInterface  {

  protected $routeMatch;
  protected $murmsPluginManager;

  function __construct(\Drupal\Core\Routing\RouteMatchInterface $route_match, $murmurations_plugin_manager) {
    $this->routeMatch = $route_match;
    $this->murmsPluginManager = $murmurations_plugin_manager;
  }

  /**
   * Arguments need to be named by the entity type or the route parameter needs to be called 'entity'.
   */
   public function access(int $entity) {
    $plugin_id = $this->routeMatch->getRouteObject()->getOption('plugin');
    $plugin = $this->murmsPluginManager->createInstance($plugin_id)->setEntity($entity);
    // Unfortunately if this returns FALSE my browser redirects to the same page and gets stuck in a loop.
    return AccessResult::allowedIf($plugin->publishable())->addCacheableDependency($entity);
  }

}
