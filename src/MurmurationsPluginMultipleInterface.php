<?php
namespace Drupal\murmurations;
use Drupal\Core\Entity\EntityInterface;

interface MurmurationsPluginMultipleInterface extends MurmurationsPluginInterface{

  /**
   * @return self
   */
  function setEntity(EntityInterface $entity) : self;

  /**
   * Get the ids of the entities to share.
   *
   * @return array
   */
  function getEntityIds() : array;


  /**
   * return TRUE if the entity should be published on the murmurations network.
   * @param type $entity
   * @return bool
   */
  function publishable() : bool;

}

