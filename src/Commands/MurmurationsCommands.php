<?php

namespace Drupal\murmurations\Commands;

use Drush\Commands\DrushCommands;
use Drupal\murmurations\MurmurationsPluginManager;

/**
 * Drush commands for murmurations module
 *
 * @package Drupal\murmurations
 */
class MurmurationsCommands extends DrushCommands {

  /**
   * @var MurmurationsPluginManager
   */
  private array $plugins;

  /**
   *
   * @param MurmurationsPluginManager $murms_plugin_manager
   */
  public function __construct(MurmurationsPluginManager $murms_plugin_manager) {
    $this->plugins = $murms_plugin_manager->allInstances();
  }

  /**
   * De-index all murmurations entries
   *
   * @command murmurations:wipe
   * @aliases mrm-wipe
   * @usage murmurations:wipe --plugin=organisation
   * @description De-index all murmurations entries. Optionally specify a plugin id.
   * @arguments
   *   plugin: The id of a murmurations plugin. (optional)
   */
  public function murmurations_wipe($plugin_id = '', $url = '') {
    if ($plugin_id) {
      $batch_definition = [
        'title' => "Deindexing $plugin_id",
        'operations' => murmurations_deindex_plugin_operations($this->plugins[$plugin_id])
      ];
    }
    else {
      foreach ($this->plugins as $plugin_id => $plugin) {
        $batch_definition = [
          'title' => 'deindexing all murmurations items',
          'operations' => murmurations_deindex_plugin_operations($plugin)
        ];
      }
    }
    batch_set($batch_definition);
    drush_backend_batch_process();
    $this->output()->writeLn('Batch executed');
  }

  /**
   * Re-index all murmurations entries
   *
   * @command murmurations:write
   * @aliases mrm-write
   * @usage murmurations:write --plugin=organisation
   * @description Insert/update all murmurations references
   * @arguments
   *   plugin: The id of a murmurations plugin. (optional)
   *
   * If drush not working, do this:
   * @code
   * $plugins = \Drupal::service('plugin.manager.murmurations')->allInstances();
   * foreach ($plugins as $plugin_id => $plugin) {
   *   $batch_definition = [
   *     'title' => 'Reindexing all murmurations items',
   *     'operations' => murmurations_reindex_plugin_operations($plugin)
   *   ];
   * }
   * batch_set($batch_definition);
   * @endcode
   */
  public function murmurations_write($plugin_id = '') {
    if ($plugin_id) {
      $batch_definition = [
        'title' => "Reindexing $plugin_id",
        'operations' => murmurations_reindex_plugin_operations($this->plugins[$plugin_id])
      ];
    }
    else {
      foreach ($this->plugins as $plugin_id => $plugin) {
        $batch_definition = [
          'title' => 'Reindexing all murmurations items',
          'operations' => murmurations_reindex_plugin_operations($plugin)
        ];
      }
    }
    batch_set($batch_definition);
    drush_backend_batch_process();
  }

}

