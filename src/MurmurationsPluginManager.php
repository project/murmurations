<?php
namespace Drupal\murmurations;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\murmurations\MurmurationsPluginBase;

/**
 * Provides a murmurations plugin manager.
 *
 * @ingroup plugin_api
 */
class MurmurationsPluginManager extends DefaultPluginManager {

  /**
   * Constructs a Murmurations Plugin manager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Murmurations',
      $namespaces,
      $module_handler,
      MurmurationsPluginInterface::class,
      Attribute\Murmurations::class,
      'Drupal\murmurations\Annotation\Murmurations'
    );
    $this->setCacheBackend($cache_backend, 'murmurations');
  }

  /**
   * Find and load a plugin for an entity type.
   * @param string $entity_type
   * @return MurmurationsPluginBase|NULL
   */
  function getByEntityType(string $entity_type) {
    foreach ($this->getDefinitions() as $id => $def) {
      if (isset($def['entity_type']) and $def['entity_type'] == $entity_type) {
        return $this->createInstance($id);
      }
    }
  }

  /**
   * Utility
   * @return \Drupal\murmurations\MurmurationsPluginBase
   */
  public function allInstances() {
    $instances = [];
    foreach ($this->getDefinitions() as $id => $def) {
      $instances[$id] = $this->createInstance($id);
    }
    return $instances;
  }

  /**
   * Utility
   * @return string[]
   */
  function getAllProfilePaths() {
    $paths = [];
    foreach ($this->allInstances() as $plugin) {
      if ($plugin->needed()) {
        continue;
      }
      $paths = array_merge($paths, $plugin->getProfilePaths());
    }
    return $paths;
  }

  /**
   * {@inheritDoc}
   */
  function createInstance($plugin_id, array $configuration = []) {
    $plugins_settings = \Drupal::config('murmurations.settings')->get('plugins');
    $plugin_settings = $plugins_settings[$plugin_id]??[];
    return parent::createInstance($plugin_id, $configuration + $plugin_settings);
  }

}
