<?php

namespace Drupal\murmurations;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
/**
 * Returns responses for murmurations routes.
 */
class Page extends \Drupal\Core\Controller\ControllerBase {

  /**
   * Make a json page for the murmurations index.
   *
   * @param Request $request
   * @return JsonResponse
   */
  function publishJson(Request $request) {
    $route = $request->attributes->get('_route_object');
    $plugin_id = $route->getOption('plugin');
    $plugin = \Drupal::service('plugin.manager.murmurations')->createInstance($plugin_id);
    if ($plugin instanceOf MurmurationsPluginMultipleInterface) {
      $entity_type = $plugin->getPluginDefinition()['entity_type'];
      $entity = $request->attributes->get('entity'); // entity_id taken from the route path
      $profile = $plugin->setEntity($entity)->getProfile();
    }
    else {
      $profile = $plugin->getProfile();
    }
    // output to browser
    header('Content-Type: text/json; charset=utf-8');
    header('Access-Control-Allow-Origin: *');
    return new JsonResponse($profile);
  }

  /**
   * The published json response to send to the aggregator.
   */
  public function published(ContentEntityType $entity) {
    $profile = murmurations_get_profile($entity);
    return JsonResponse::create(murmurations_get_profile($entity));
  }

  /**
   * Show the entity as json on a page for no particular reason.
   */
  public function view(ContentEntityType $entity) {
    $profile = murmurations_get_profile($entity);
    $html = '<p>'.t("This is shared with the murmurations aggregator if the visibility of the item is set to 'network'.").'</p>';
    $html .= '<pre>'.json_encode($profile, JSON_PRETTY_PRINT).'</pre>';
    return [
      '#markup' => $html,
    ];
  }

  /**
   * {@inheritDoc}
   */
  function filterPageTitle(Request $request) {
    return $this->t(
      '@type filter',
      ['@type' => $this->murmPlugin($request)->getPluginDefinition()['label']]
    );
  }

  /**
   * {@inheritDoc}
   */
  public function __searchMap(Request $request) {
    $params = $request->query->all();
    $renderable['form'] = $this->formBuilder()
      ->getForm('\Drupal\murmurations\Form\FilterFormMap', $params);
// Reconsider where those functions should go. e.g. getFilter
$form_state = new \Drupal\Core\Form\FormState();
$this->formBuilder->getFormId('\Drupal\murmurations\Form\FilterFormMap', $form_state);
$form_object = $form_state->getFormObject();

    if ($params) {
      $plugin = $this->murmPlugin($request);
      $filters = $form_object->getFilters($params);
      // Now submit the search results.
      $results = $form_object->searchRequest($filters);
      if (isset($results->data)) {
        $this->total = $results->meta->number_of_results;
        $this->pages = $results->meta->total_pages;
        $items = $results->data;// not sure why this is nested.
      }
      else {
        $items = [];
      }
      $renderable['results'] = $form_object->showResults($items, -1);
    }
    return $renderable;
  }

  /**
   * {@inheritDoc}
   */
  public function __searchList(Request $request) {
    $params = $request->query->all();
    $renderable['form'] = $this->formBuilder()
      ->getForm('\Drupal\murmurations\Form\FilterFormList', $params);

// Reconsider where those functions should go. e.g. getFilter
$form_state = new \Drupal\Core\Form\FormState();
$this->formBuilder->getFormId('\Drupal\murmurations\Form\FilterFormList', $form_state);
$form_object = $form_state->getFormObject();

    if ($params) {
      $plugin = $this->murmPlugin($request);
      $filters = $form_object->getFilters($params);
      // Now submit the search results.
      $results = $form_object->searchRequest($filters);
      if (isset($results->data)) {
        $this->total = $results->meta->number_of_results;
        $this->pages = $results->meta->total_pages;
        $items = $results->data;// not sure why this is nested.
      }
      else {
        $items = [];
      }
      $renderable['results'] = $form_object->showResults($items, -1);
    }
    return $renderable;
  }

  /**
   * @param Request $request
   * @return type
   */
  private function murmPlugin(Request $request) {
    $plugin_id = $request->attributes->get('_route_object')->getOptions()['plugin'];
    return \Drupal::service('plugin.manager.murmurations')->createInstance($plugin_id);
  }

}
