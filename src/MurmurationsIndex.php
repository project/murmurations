<?php

namespace Drupal\murmurations;

use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use Drupal\Core\Config\ConfigFactory;
use GuzzleHttp\Client;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Logger\LoggerChannel;

/**
 * Drupal service to communicate with the murmurations index.
 * @see https://app.swaggerhub.com/apis-docs/MurmurationsNetwork/IndexAPI/2.0.0
 */
class MurmurationsIndex {

  private $requester;
  private $messenger;
  private $logger;
  protected $indexUrl;

  /**
   * @param ConfigFactory $config_factory
   * @param Client $http_client
   * @param Messenger $messenger
   * @param LoggerChannel $logger
   */
  function __construct(ConfigFactory $config_factory, Client $http_client,  Messenger $messenger, LoggerChannel $logger) {
    $this->requester = $http_client;
    $this->messenger = $messenger;
    $this->logger = $logger;
    $this->indexUrl = $config_factory->get('murmurations.settings')->get('index_url');
  }

  /**
   * Add or update an item.
   *
   * @param string $profile_rel_url
   *
   * @return bool
   *   TRUE on success
   */
  function reindex(string $profile_rel_url) : bool {
    $abs_url = $this->absUrl($profile_rel_url);
    $options = [
      RequestOptions::BODY => json_encode(['profile_url' => $abs_url]),
      RequestOptions::TIMEOUT => 5
    ];
    $murm_url = $this->indexUrl . 'nodes-sync';
    try {
      // If this times out then nothing catches it.
      $response = $this->requester->post($murm_url, $options);
    }
    catch (RequestException $e) {
      $this->logger->error('RequestException on submitting @url: @message', ['@message' => $e->getMessage(), '@url' => $abs_url]);
      //$this->messenger->addError("There was a problem submitting $profile_rel_url to the murmurations index. See log for details.");
      return FALSE;
    }
    catch (ClientException $e) {
      // An error as specified in the murmurations API
      $response = $e->getResponse();
      $answer = json_decode($response->getBody()->getContents());
      foreach ($answer->errors as $error) {
        if (isset($error->source)) {
          $this->logger->error(
            "Murmurations rejected the item at @pointer. @reason<br />@posted",
            [
              '@pointer' => $error->source->pointer,
              '@reason' => $error->detail,
              '@posted' => print_r($options, 1)
            ]
          );
        }
        else {
          $this->logger->error(
            "Murmurations rejected the item, responding with <pre>@data</pre>",
            [
              '@data' => print_r($error, 1)
            ]
          );
        }
      }
      // This is truncated
      $this->messenger->addError($e->getMessage());
      return FALSE;
    }
    catch (ServerException $e) { // Problem with the murmurations server
      $this->messenger->addError($e->getMessage());
      switch ($e->getCode()) {
        case 500:
          $answer = json_decode($e->getResponse()->getBody()->getContents());
          $this->logger->error($answer->errors->detail);
          break;
        default:
          $this->logger->error('@status_code @error', [
            '@status_code' => $e->getCode(),
            '@error' => $e->getMessage()
          ]);
      }
      return FALSE;
    }
    catch (ConnectException $e) {
      $this->logger->error('Unable to connect to murmurations: POST @body<br />@message', [
        '@body' => $this->options[RequestOptions::BODY] ?? '',
        '@message' => $e->getMessage()
      ]);
      return FALSE;
    }
    catch (\Exception $e) {
      $this->logger->error(
        'Unexpected '.get_class($e).' error: '.$e->getMessage(),
        ['@message' => $e->getMessage()]
      );
      return FALSE;
    }
    $this->messenger->addStatus(t('Your item is published on the network.'));
    return TRUE;
  }

  /**
   * Remove an item from the index
   *
   * @return boolMyServiceInterface
   *   TRUE on success
   * @throws Exception
   */
  function deindex($profile_rel_url) {
    $node_id = hash('SHA256', $this->absUrl($profile_rel_url));
    $url = $this->indexUrl . 'nodes/'.$node_id;
    try {
      $response = $this->requester->delete($url);
      $this->messenger->addStatus(t('Your ad has been removed from the network.'));
    }
    catch (ClientException $e) { }
    catch (BadResponseException $e) {// 4xx, 5xx
      $this->logger->warning('@message', ['@message' => $e->getMessage()]);
      throw $e;
    }
  }

  /**
   * Convert a relative profile url to an absolute url for the murmurations index.
   * @param string $rel_url
   * @return string
   */
  private function absUrl(string $rel_url) : string {
    // Build the absolute string using the file generator which tries to determine the scheme from the request url
    // from drush however this is likely to return http which is likely incorrect
    return \Drupal::service('file_url_generator')->generateAbsoluteString($rel_url);
  }

}
