<?php

namespace Drupal\murmurations\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use \Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a Murmruations attribute.
 *
 * Plugin Namespace: Plugin\murmurations
 *
 * @ingroup migration
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class Murmurations extends Plugin {

  /**
   * Constructs a migrate destination plugin attribute object.
   *
   * @param string $id
   *   A unique identifier for the destination plugin.
   * @param bool $label
   * @param string $schema
   *   the official string of the schema and version number.
   * @param string $profile_path
   *   The internal path or path pattern.
   * @param string $config
   *   The route id for the config page
   * @param string $default_aggregator
   *   Url to the default aggregator.
   * @param string $entity_type
   *   The machine name of the entity (optional)
   */
  public function __construct(
    public readonly string $id,
    public TranslatableMarkup $label,
    public string $schema,
    public string $profile_path,
    public string $config,
    public string $default_aggregator,
    public ?string $entity_type = NULL,
  ) {
  }

}
