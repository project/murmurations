<?php
namespace Drupal\murmurations\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * @Annotation
 */
class Murmurations extends Plugin {

  /**
   * The machine name of the plugin
   * @var string
   */
  public $id;

  /**
   * The human name of the plugin
   * @var string
   */
  public $label;
  /**
   * The murmurations schema and version number
   * @var string
   */
  public $schema;

  /**
   * Optional if the plugin is producing one profile per entity. Use the symfony
   * syntax for variables i.e. node/{node}/view
   * @var string
   */
  public $profile_path;

  /**
   * The name of the config where the settings, if any, should be saved.
   * @var string
   */
  public $config;

  /**
   * An aggregator which works with this schema (only needed for search interface)
   * @var string
   */
  public $default_aggregator;
}
