<?php

namespace Drupal\murmurations\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\murmurations\MurmurationsPluginManager;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

/**
 * Make a profile route and map and list routes for each profile.
 * @todo use the plugin definition to declare the root path for the search tabs.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * @var MurmurationsPluginManager
   */
  protected $pluginManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(MurmurationsPluginManager $murmurations_plugin_manager) {
    $this->pluginManager = $murmurations_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach($this->pluginManager->allInstances() as $id => $plugin) {
      if ($plugin->needed()) {
        continue;
      }
      $plugin_def = $plugin->getPluginDefinition();
      $profile_path = $plugin_def['profile_path'];
      if (isset($plugin_def['entity_type'])) {
        $profile_path = str_replace($plugin_def['entity_type'], 'entity', $profile_path);
      }
      // One route to display the json profiles for each plugin
      $route = new Route(
        $profile_path,
        ['_controller' => 'Drupal\murmurations\Page::publishJson'],
        [],
        ['plugin' => $id, '_format' => 'json']
      );
      if (str_contains($profile_path, '{')) {
        $route->setRequirement('_murmurations_profile_published', 'true');
      }
      else {
        $route->setOption('_access', 'TRUE');
      }
      $collection->add("murmurations.$id", $route);

      // Two routes for the map and list filter pages for each plugin
      $collection->add(
        "murmurations.$id.search.map",
        new Route(
          "murmurations/$id",
          [
            '_form' => '\Drupal\murmurations\Form\FilterFormMap',
            '_title_callback' => '\Drupal\murmurations\Page::filterPageTitle'
          ],
          ['_permission' =>  'filter murmurations content'],
          ['plugin' => $id]
        )
      );
      $collection->add(
        "murmurations.$id.search.list",
        new Route(
          "murmurations/$id/list",
          [
            '_form' => '\Drupal\murmurations\Form\FilterFormList',
            '_title_callback' => '\Drupal\murmurations\Page::filterPageTitle'
          ],
          ['_permission' =>  'filter murmurations content'],
          ['plugin' => $id]
        )
      );
    }
  }
}
