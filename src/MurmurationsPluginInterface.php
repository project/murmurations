<?php
namespace Drupal\murmurations;

interface MurmurationsPluginInterface {

  /**
   * Convert the data object (usually a contentEntity) into a murmurations
   * profile. Validate using https://www.jsonschemavalidator.net and the schema
   * from https://github.com/MurmurationsNetwork/MurmurationsLibrary/tree/test/schemas
   *
   * @param \stdClass $object
   *
   * @return \stdClass
   */
  function getProfile() : array;

  /**
   * @return string
   *   Reason(s) why the plugin is not ready.
   */
  function needed() : string;

  /**
   * @return array
   *
   * @todo I think there's another interface for configurable plugins..
   */
  function configForm() : array;

  /**
   * Get the relative murmurations path of the given entity, if the entity should be published.
   *
   * @param $entity
   *
   * @return string
   */
  function getPublishPath() : string;

  /**
   * Get the urls of [all the] json files for this plugin.
   *
   * @return array
   */
  public function getProfilePaths() : array;

  /**
   * Generate the body of the search results.
   *
   * @param \stdClass $result
   *
   * @return \Drupal\Core\Render\Markup
   *
   * @note the search result over all is themed by murm_search_result_text
   */
  function renderResult(\stdClass $result) : \Drupal\Core\Render\Markup;


  /**
   * Get the coordinates at which to display the object.
   *
   * @return array
   *   with float 'lat' and float 'lon'
   */
  function getCoords() : array;

}
