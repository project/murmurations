<?php

namespace Drupal\murmurations\Form;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\leaflet\LeafletService;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\murmurations\MurmurationsPluginManager;
use Drupal\Core\Form\FormStateInterface;

/**
 * Inject Leaflet service
 */
class FilterFormMap extends FilterForm {

  const MAP_WIDTH = '400px';
  const RESULTS_PER_PAGE = 50;

  /**
   * @var LeafletService
   */
  protected $leaflet;

  /**
   * @var ExtensionPathResolver
   */
  protected $pathResolver;

 /**
  * @param type $user_data
  * @param type $request
  * @param type $route_match
  * @param MurmurationsPluginManager $murm_plugin_manager
  * @param Drupal\leaflet\LeafletService $leaflet_service
  * @param Drupal\Core\Extension\ExtensionPathResolver $path_resolver
  */
  public function __construct($user_data, $request, $route_match, MurmurationsPluginManager $murm_plugin_manager, LeafletService $leaflet_service, ExtensionPathResolver $path_resolver) {
    parent::__construct($user_data, $request, $route_match, $murm_plugin_manager);
    $this->leaflet = $leaflet_service;
    $this->pathResolver = $path_resolver;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.data'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('current_route_match'),
      $container->get('plugin.manager.murmurations'),
      $container->get('leaflet.service'),
      $container->get('extension.path.resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'murmurations_filter_form_map';
  }


  function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    if (isset($form['results'])) {
      [$items, $metadata] = $this->searchRequest();
      if ($items and $metadata->number_of_results > count($items)) {
        $form['results']['#title'] = t('Showing nearest @count results from @total.', ['@count' => count($items), '@total' => $metadata->number_of_results]);
      }
    }
    return $form;
  }

  /**
   * Show the results in a map.
   *
   * @param array $results
   * @param int $number_of_results
   * @return array
   */
  public function showResults(array $results, int $number_of_results) : array {
    $features = [];
    foreach ($results as $result) {
      if (!isset($result->geolocation)) {
        continue;
      }
      $header = Link::fromTextAndUrl($result->title, Url::fromUri($result->primary_url))->toString();
      $popup = $header .'<br />'.
        text_summary($result->description, filter_fallback_format(), 150).'<br />'.
        $result->tags;
      $features[] = [
        'type' => 'point',
        'lat' => $result->geolocation->lat,
        'lon' => $result->geolocation->lon,
        'icon' => [
          'iconUrl' => '/'.$this->pathResolver->getPath('module', 'murmurations') . '/images/bluepin.png',
          'icon_anchor' => ['x' => 24, 'y' => 32],//not supported yet in leaflet module Beta1
          'popup_anchor' => ['x' => 16, 'y' => 0],
        ],
        'popup' => [
          'value' => $popup,
          'options' => "{}", // {"maxWidth":"300","minWidth":"50","autoPan":true}
          'control' => 1
        ],
        'leaflet_id' => md5($result->primary_url)
      ];
    }
    if ($features) {
      // The map will try to centre around the data by default.
      return $this->leaflet->leafletRenderMap(
        leaflet_leaflet_map_info()['OSM Mapnik'],
        $features,
        SELF::MAP_WIDTH
      );
    }
    return [];
  }

}
