<?php

namespace Drupal\murmurations\Form;

use Drupal\murmurations\MurmurationsPluginManager;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Locale\CountryManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SettingsForm extends FormBase {

  /**
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var MurmurationsPluginManager
   */
  protected $murmPluginManager;

  /**
   * @var CountryManager
   */
  protected $countryManager;

  /**
   * @param EntityTypeManager $entity_type_manager
   * @param MurmurationsPluginManager $plugin_manager_murmurations
   * @param CountryManager $country_manager
   */
  public function __construct(EntityTypeManager $entity_type_manager, MurmurationsPluginManager $plugin_manager_murmurations, CountryManager $country_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->murmPluginManager = $plugin_manager_murmurations;
    $this->countryManager = $country_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.murmurations'),
      $container->get('country_manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('murmurations.settings');

    $form['index_url'] = [
      '#title' => $this->t('Url of murmurations index'),
      '#placeholder' => 'https://index.murmurations.network/v2/',
      '#type' => 'textfield',
      '#default_value' => $config->get('index_url'),
      '#pattern' => 'https?:\/\/[a-z0-9/.\-]+\/',
      '#attributes' => ['title' => "A url with a trailing slash"],
      '#weight' => 1
    ];
    $form['fallback_point'] = [
      '#title' => $this->t('Fallback location'),
      '#description' => t('Geo location for items in the global index, from members who are not geolocated.'),
      '#type' => 'details',
      '#open' => TRUE,
      '#tree' => TRUE,
      '#weight' => 3,
      'lat' => [
        '#title' => $this->t('Latitude'),
        '#type' => 'textfield',
        '#default_value' => $config->get('fallback_point.lat'),
        '#placeholder' => '0.0',
        '#required' => TRUE,
        '#weight' => 1,
      ],
      'lon' => [
        '#title' => $this->t('Longitude'),
        '#type' => 'textfield',
        '#default_value' => $config->get('fallback_point.lon'),
        '#placeholder' => '0.0',
        '#required' => TRUE,
        '#weight' => 2,
      ]
    ];

    $form['locality'] = [
      '#title' => $this->t('Locality'),
      '#type' => 'textfield',
      '#default_value' => $config->get('locality'),
      '#weight' => 4
    ];
    $form['region'] = [
      '#title' => $this->t('Region'),
      '#type' => 'textfield',
      '#default_value' => $config->get('region'),
      '#weight' => 5
    ];

    // Put this here just for completeness
    $country_list= $this->countryManager->getList();
    $country_code = $this->config('system.date')->get('country')['default'];

    $form['country'] = [
      '#title' => $this->t('Country'),
      '#type' => 'select',
      '#default_value' => $country_code,
      '#options' => $country_list,
      '#weight' => 6,
      '#disabled' => !empty($country_code)
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#weight' => 10
    ];
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $index_url = $form_state->getValue('index_url');
    $settings = $this->configFactory()->getEditable('murmurations.settings');
    $settings->set('index_url', $index_url)
      ->set('fallback_point', $form_state->getValue('fallback_point'))
      ->set('locality', $form_state->getValue('locality'))
      ->set('region', $form_state->getValue('region'))
      ->set('country', $form_state->getValue('country'))
      ->save();
    if ($paths = $this->murmPluginManager->getAllProfilePaths()) {
      murmurations_queue_items($paths);
      \Drupal::messenger()->addStatus(
        t('@count items have been queued for re-indexing (via cron)', ['@count' => count($paths)])
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'murmurations_settings';
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return ['murmurations.settings'];
  }

}
