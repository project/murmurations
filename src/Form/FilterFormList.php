<?php

namespace Drupal\murmurations\Form;

use Drupal\murmurations\MurmurationsPluginManager;
use Drupal\Core\Pager\PagerManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FilterFormList extends FilterForm {

  const RESULTS_PER_PAGE = 10;//make this a setting

  /**
   * Total number of search results
   *
   * @var int
   */
  protected $total;

  /**
   * @var PagerManager
   */
  protected $pager_manager;

   /**
  *
  * @param type $user_data
  * @param type $request
  * @param type $route_match
  * @param MurmurationsPluginManager $murm_plugin_manager
  * @param Drupal\Core\Pager\PagerManager $pager_manager
  */
  public function __construct($user_data, $request, $route_match, MurmurationsPluginManager $murm_plugin_manager, PagerManager $pager_manager) {
    parent::__construct($user_data, $request, $route_match, $murm_plugin_manager);
    $this->pagerManager = $pager_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.data'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('current_route_match'),
      $container->get('plugin.manager.murmurations'),
      $container->get('pager.manager')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'murmurations_filter_form_list';
  }

  /**
   * Construct the query string, send, and return the result items
   * @param $params
   *   A non empty list
   * @return array
   */
  public function getFilters(array $params) : array {
    $filters = parent::getFilters($params);
    if (isset($params['page'])) {
      $filters['page'] = $params['page'];
    }
        // Generic search filters
    $filters['page_size'] = self::RESULTS_PER_PAGE;// Make this an advanced form setting
    if (isset($params['fragment'])) {
      $filters['text'] = $params['fragment'];
    }
    return $filters;
  }

  /**
   * Show the results in a list, with pager.
   *
   * @param array $results
   * @param int $number_of_results
   * @return array
   */
  public function showResults(array $results, int $number_of_results) : array {
    $form = [];
    foreach ($results as $id => $result) {
      // them 'search_result' might not be flexible enough for our purposes.
      $form[$id] = [
        '#theme' => 'murm_search_result_text',
        '#plugin'=> $this->plugin->getPluginId(),
        '#title' => $result->title,
        // We don't know which text format we can use, so must assume plain_text i.e. no html.
        '#body' => $this->plugin->renderResult($result),//text_summary($result->description, filter_fallback_format(), 400),
        '#url' => $result->primary_url,
        '#tags' => $result->tags,
        '#distance' => round($result->distance, 1),
        '#weight' => $id +10
      ];
    }

    $pager = $this->pagerManager->createPager(
      $number_of_results,
      static::RESULTS_PER_PAGE
    );
    $form['pager'] = [
      '#type' => 'pager',
      '#weight' => $id + 11,
    ];
    return $form;
  }

}
