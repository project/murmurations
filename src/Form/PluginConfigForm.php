<?php

namespace Drupal\murmurations\Form;

use Drupal\murmurations\MurmurationsPluginManager;
use Drupal\Core\Render\Markup;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PluginConfigForm extends ConfigFormBase implements ContainerInjectionInterface {

  protected $pluginManager;
  protected $pluginId;
  protected $plugin;

  /**
   * @param MurmurationsPluginManager $murmurations_plugin_manager
   * @param CurrentRouteMatch $request
   */
  public function __construct(MurmurationsPluginManager $murmurations_plugin_manager, CurrentRouteMatch $route_match, ConfigFactoryInterface $config_factory) {
    $this->pluginId = $route_match->getParameter('id');
    $this->plugin = $murmurations_plugin_manager->createInstance($this->pluginId);
    if ($conf_name = $this->configurable()) {
      $this->plugin->setConfiguration($config_factory->get($conf_name)->getRawData());
    }
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.murmurations'),
      $container->get('current_route_match'),
      $container->get('config.factory')
    );
  }


  /**
   * {@inheritDoc}
   */
  function buildForm(array $form, FormStateInterface $form_state) {
    $schema = $this->plugin->getpluginDefinition()['schema'];
    if ($reason = $this->plugin->needed()) {
      $schema .= '<br /><font color="red">'.$reason.'</font>';
    }
    $form['version'] = [
      '#type' => 'item',
      '#title' => 'Schema:',
      '#markup' => Markup::Create($schema),
      '#weight' => 0
    ];
    $aggs = $this->config('murmurations.settings')->get('plugins');
    $form['aggregator_url'] = [
      '#title' => 'Aggregator url',
      '#description' => $this->t('If no url is provided the main index will be searched instead, which is unaware of most of the fields.'),
      '#pattern' => 'https?:\/\/[a-z0-9/.\-]+\/?',
      '#type' => 'textfield',
      '#default_value' => $aggs[$this->pluginId]['aggregator_url']??$this->plugin->getpluginDefinition()['default_aggregator'],
      '#placeholder' => $this->config('murmurations.settings')->get('index_url'),
      '#element_validate' => [[$this, 'addTrailingSlash']],
      '#weight' => 1
    ];
    $form['displacement'] = [
      '#title' => $this->t('Displace map points (can help with privacy)'),
      '#description' => $this->t('Plus or minus thousandths of a degree of latitude or longitude.'),
      '#type' => 'number',
      '#min' => 0,
      '#max'=> 20,
      '#size'=> 4,
      '#default_value' => $aggs[$this->pluginId]['displacement']??'',
    ];
    if ($this->configurable()) {
      //$plugin_config = $this->configFactory->get($this->plugin->configName());
      $form['settings'] = [
        '#title' => $this->t('Settings'),
        '#tree' => 1,
        '#type' => 'fieldset',
        '#weight' => 5
      ] + $this->plugin->configForm();
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit and reindex'),
      '#weight' => 10
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  function submitForm(&$form, FormStateInterface $form_state) {
    $murm_config = $this->configFactory()->getEditable('murmurations.settings');
    $aggs = $murm_config->get('plugins');
    $aggs[$this->pluginId] = [
      'aggregator_url' => $form_state->getValue('aggregator_url'),
      'displacement' => $form_state->getValue('displacement')
    ];
    $murm_config->set('plugins', $aggs)
      ->save();
    if ($config_name = $this->configurable()) {
      $plugin_conf = $this->configFactory()->getEditable($config_name);
      if ($settings = $form_state->getValue('settings')) {
        foreach ($settings as $key => $value) {
          if (is_array($value)) {
            $value = array_filter(array_values($value));
          }
          $plugin_conf->set($key, $value);
        }
      }
      $plugin_conf->save();
    }
    if ($paths = $this->plugin->getProfilePaths()) {
      murmurations_queue_items($this->plugin->getProfilePaths());
      \Drupal::messenger()->addStatus(
        t('@count items have been queued for re-indexing (via cron)', ['@count' => count($paths)])
      );
    }

  }

  private function configurable() : string {
    $def = $this->plugin->getPluginDefinition();
    return $def['config'] ?? '';
  }

  /**
   * {@inheritDoc}
   */
  function getFormId() {
    return 'murmurations_config_'.$this->pluginId;
  }

  /**
   * {@inheritDoc}
   */
  function getEditableConfigNames() {
    $names = ['murmurations.settings'];
    if ($config = $this->configurable()) {
      $names[] = $config;
    }
    return $names;
  }

  function addTrailingSlash(&$element, $form_state) {
    if (substr($element['#value'], -1) <> '/') {
      $form_state->setValue($element['#name'], $element['#value'] .'/');
    }
  }
}
